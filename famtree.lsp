#|--------------------------------------------------------------------------|
               ______              _ _         _______                      |
              |  ____|            (_) |       |__   __|                     |
              | |__ __ _ _ __ ___  _| |_   _     | |_ __ ___  ___           |
              |  __/ _` | '_ ` _ \| | | | | |    | | '__/ _ \/ _ \          |
              | | | (_| | | | | | | | | |_| |    | | | |  __/  __/          |
              |_|  \__,_|_| |_| |_|_|_|\__, |    |_|_|  \___|\___|          |
                                        __/ |                               |
                                       |___/                                |
#|--------------------------------------------------------------------------|

    CSC 461 Program 2: Family Tree
    Authors: Brock Benson & Alex Muchow
    Purpose: To load in a database containing the attributes of an individual.
             Each individual has a gender, children, and parents, and the 
             program can make connections of relationships based on these
             attributes. The program starts off by loading in your database,
             and then it allows you to make queries about said databse. The
             queries that can be made are: parents, mothers, fathers, children,
             sons, daughters, siblings, sisters, brothers, grandparents, 
             grandfathers, grandmothers, grandchildren, grandsons,granddaughters,
             uncles, aunts, nieces, nephews, cousins, male-cousins, female-cousins,
             ancestors, male-ancestors, female-ancestors, descendants,
             male-descendants, and female-descendants.


|#--------------------------------------------------------------------|#


#|--------------------------------------------------------------------------|
   _____ _       _           _           _____ _                   _        |
  / ____| |     | |         | |      _  / ____| |                 | |       |
 | |  __| | ___ | |__   __ _| |___ _| || (___ | |_ _ __ _   _  ___| |_ ___  |
 | | |_ | |/ _ \| '_ \ / _` | / __|_   _\___ \| __| '__| | | |/ __| __/ __| |
 | |__| | | (_) | |_) | (_| | \__ \ |_| ____) | |_| |  | |_| | (__| |_\__ \ |
  \_____|_|\___/|_.__/ \__,_|_|___/    |_____/ \__|_|   \__,_|\___|\__|___/ |
                                                                            |
                                                                            |
#|--------------------------------------------------------------------------|

    Global: *database* - Holds a list of people from the database file
    Struct:    person  - contains information about an individual
            - name, sex, children, parents

|#--------------------------------------------------------------------|#

(defvar *database* (list))
(defstruct person name sex children parents)


#|---------------------------------------------------------------------|
  _______        _      _____                _ _ _   _                 | 
 |__   __|      | |    / ____|              | (_) | (_)                |
    | | ___  ___| |_  | |     ___  _ __   __| |_| |_ _  ___  _ __  ___ |
    | |/ _ \/ __| __| | |    / _ \| '_ \ / _` | | __| |/ _ \| '_ \/ __||
    | |  __/\__ \ |_  | |___| (_) | | | | (_| | | |_| | (_) | | | \__ \|
    |_|\___||___/\__|  \_____\___/|_| |_|\__,_|_|\__|_|\___/|_| |_|___/|
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|

    Function Names: equalnames, equalsex
    Purpose: equalnames is used to locate a person in the database
         equalsex is used by filters to determine the gennder
            of a person
|#--------------------------------------------------------------------|#
(defun equalnames (name s1)
    (if (equal name (person-name s1)) (return-from equalnames T))
)

(defun equalsex (name s1)
    (if (equal (person-sex (nsearch name)) (person-sex (nsearch s1))) (return-from equalsex T))
)


#|----------------------------------------------------------------------|
   _____                     _          ______ _ _ _                    |
  / ____|                   | |     _  |  ____(_) | |                   |
 | (___   ___  __ _ _ __ ___| |__ _| |_| |__   _| | |_ ___ _ __ ___     |
  \___ \ / _ \/ _` | '__/ __| '_ \_   _|  __| | | | __/ _ \ '__/ __|    |
  ____) |  __/ (_| | | | (__| | | ||_| | |    | | | ||  __/ |  \__ \    |
 |_____/ \___|\__,_|_|  \___|_| |_|    |_|    |_|_|\__\___|_|  |___/    |
                                                                        |
                                                                        |
#|----------------------------------------------------------------------|

    Function Names: nsearch, malefilter, femalefilter
    Purpose: nsearch finds a person in the database
                malefilter removes all females from a list
                femalefilter removes all males from a list

|#--------------------------------------------------------------------|#
(defun nsearch (name)
    (setf name (find name *database* :test #'equalnames))
)

(defun malefilter (plist)
    (dolist (i plist)
        (if (not (equal (person-sex (nsearch i)) 'male) ) (setf plist (remove i plist :test #'equalsex)))
    )
    (return-from malefilter plist)
)

(defun femalefilter (plist)
    (dolist (i plist)
        (if (not (equal (person-sex (nsearch i)) 'female) ) (setf plist (remove i plist :test #'equalsex)))
    )
    (return-from femalefilter plist)
)


#|---------------------------------------------------------------------|
                  _____        _        _                              |
                 |  __ \      | |      | |                             |
                 | |  | | __ _| |_ __ _| |__   __ _ ___  ___           |
                 | |  | |/ _` | __/ _` | '_ \ / _` / __|/ _ \          |
                 | |__| | (_| | || (_| | |_) | (_| \__ \  __/          |
                 |_____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|          |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|

    Function Names: buildDatabase
    Purpose: Creates a global list called *database* which contains all
                persons in the supplied file


    Author: Weiss

|#--------------------------------------------------------------------|#
(defun buildDatabase (filename)

    ;given an input file, create a database stored in the *database* variable
    (format t "Building Database from ~s~%" (first *ARGS*))    

    ;Read through file using with-open-file
    (with-open-file (fin filename :if-does-not-exist nil)
        (when (null fin) (return-from fileio (format nil "Error: cannot open file ~a" filename)))
            (do ((data (read fin nil) (read fin nil)))      ; read entire file, returning NIL at EOF
            ((null data) 'done)                         ; exit when file is read
            (setf temp (make-person :name (first data) :sex (second data) :children (third data) :parents (fourth data)))
            (push temp *database*)
        )
    )
)

#|---------------------------------------------------------------------|
                 _____                     _                           |
                |  __ \                   | |                          |
                | |__) |_ _ _ __ ___ _ __ | |_ ___                     |
                |  ___/ _` | '__/ _ \ '_ \| __/ __|                    |
                | |  | (_| | | |  __/ | | | |_\__ \                    |
                |_|   \__,_|_|  \___|_| |_|\__|___/                    |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|

    Function Names: parents, mothers, fathers
    Purpose: find the parents of a given name, can be filtered by
        male or female

|#--------------------------------------------------------------------|#
(defun parents (name)
    (if (not (equal (nsearch name) nil))
        (return-from parents (person-parents (nsearch name)))
    )
)
(defun mothers (name)
    (return-from mothers (femalefilter (parents name)))
)
(defun fathers (name)
    (return-from fathers (malefilter (parents name)))
)

#|---------------------------------------------------------------------|
                     _____ _     _ _     _                             |
                    / ____| |   (_) |   | |                            |
                   | |    | |__  _| | __| |_ __ ___ _ __               |
                   | |    | '_ \| | |/ _` | '__/ _ \ '_ \              |
                   | |____| | | | | | (_| | | |  __/ | | |             |
                    \_____|_| |_|_|_|\__,_|_|  \___|_| |_|             |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|

    Function Names: children, sons, daughters
    Purpose: find the children of a given name, can be filtered by
            male or female

|#--------------------------------------------------------------------|#
(defun children (name)
    (if (not (equal (nsearch name) nil))
        (return-from children (person-children (nsearch name)))
    )
)
(defun sons (name)
    (return-from sons (malefilter (children name)))
)
(defun daughters (name)
    (return-from daughters (femalefilter (children name)))
)


   
#|---------------------------------------------------------------------|
                  _____ _ _     _ _                                    |
                 / ____(_) |   | (_)                                   |
                | (___  _| |__ | |_ _ __   __ _ ___                    |
                 \___ \| | '_ \| | | '_ \ / _` / __|                   |
                 ____) | | |_) | | | | | | (_| \__ \                   |
                |_____/|_|_.__/|_|_|_| |_|\__, |___/                   |
                                           __/ |                       |
                                          |___/                        |
#|---------------------------------------------------------------------|

    Function Names: siblings, sisters, brothers
    Purpose: find the siblings of a given name by finding the parents
        children, can be filtered by male or female.

|#--------------------------------------------------------------------|#

(defun siblings (name)
    (if (not (equal (nsearch name) nil))
      (let (templist)
          (dolist (parent (parents name))
              (dolist (child (children parent))
                  (if (not (member child templist )) (push child templist))
              )
          )
          (setf templist (remove name templist))
          (return-from siblings templist)
      )
    )
)
(defun sisters (name)
    (return-from sisters (femalefilter (siblings name)))
)
(defun brothers (name)
    (return-from brothers (malefilter (siblings name)))
)
   
#|---------------------------------------------------------------------|
   _____                     _      _     _ _     _                    |
  / ____|                   | |    | |   (_) |   | |                   |
 | |  __ _ __ __ _ _ __   __| | ___| |__  _| | __| |_ __ ___ _ __      |
 | | |_ | '__/ _` | '_ \ / _` |/ __| '_ \| | |/ _` | '__/ _ \ '_ \     |
 | |__| | | | (_| | | | | (_| | (__| | | | | | (_| | | |  __/ | | |    |
  \_____|_|  \__,_|_| |_|\__,_|\___|_| |_|_|_|\__,_|_|  \___|_| |_|    |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|

    Function Names: grandchildren, grandsons, granddaughters
    Purpose: find the grandchildren of a given name by finding 
        the children's children, can be filtered by male or 
        female.

|#--------------------------------------------------------------------|#
(defun grandchildren (name)
    (if (not (equal (nsearch name) nil))
      (let (templist)
          (dolist (child (children name))
              (dolist (gchild (children child))        
                  (if (not (member gchild templist )) (push gchild templist))
              )
          )

          (return-from grandchildren templist)
      )
    )
)
(defun grandsons (name)
    (return-from grandsons (malefilter (grandchildren name)))
)
(defun granddaughters (name)
    (return-from granddaughters (femalefilter (grandchildren name)))
)

#|---------------------------------------------------------------------|
   _____                     _                            _            |
  / ____|                   | |                          | |           |
 | |  __ _ __ __ _ _ __   __| |_ __   __ _ _ __ ___ _ __ | |_ ___      |
 | | |_ | '__/ _` | '_ \ / _` | '_ \ / _` | '__/ _ \ '_ \| __/ __|     |
 | |__| | | | (_| | | | | (_| | |_) | (_| | | |  __/ | | | |_\__ \     |
  \_____|_|  \__,_|_| |_|\__,_| .__/ \__,_|_|  \___|_| |_|\__|___/     |
                              | |                                      |
                              |_|                                      |
#|---------------------------------------------------------------------|

    Function Names: grandparents, grandfathers, 
    Purpose: find the grandparents of a given name by finding the 
        parents of parents, can be filtered by male or female    

|#--------------------------------------------------------------------|#
(defun grandparents (name)
    (if (not (equal (nsearch name) nil))
      (let (templist)
          (dolist (parent (parents name))
              (dolist (gchild (parents parent))        
                  (if (not (member gchild templist )) (push gchild templist))
              )
          )

          (return-from grandparents templist)
      )
    )
)
(defun grandfathers (name)
    (return-from grandfathers (malefilter (grandparents name)))
)
(defun grandmothers (name)
    (return-from grandmothers (femalefilter (grandparents name)))
)

#|---------------------------------------------------------------------|
                      _             _    _            _                |
     /\              | |        _  | |  | |          | |               |
    /  \  _   _ _ __ | |_ ___ _| |_| |  | |_ __   ___| | ___  ___      |
   / /\ \| | | | '_ \| __/ __|_   _| |  | | '_ \ / __| |/ _ \/ __|     |
  / ____ \ |_| | | | | |_\__ \ |_| | |__| | | | | (__| |  __/\__ \     |
 /_/    \_\__,_|_| |_|\__|___/      \____/|_| |_|\___|_|\___||___/     |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|

    Function Names: parent-siblings, uncles, aunts
    Purpose: find the Aunts + Uncles of a given name by finding the
        parent's siblings, and their children's parents, can be 
        filtered by male or female.

|#--------------------------------------------------------------------|#
(defun parent-siblings (name)
    (if (not (equal (nsearch name) nil))
      (let (templist)
          (dolist (it1 (parents name))
              (dolist (it2 (siblings it1))    
                  (if (not (member it2 templist )) 
                      (push it2 templist))

                  (dolist (it3 (children it2))
                      (dolist (it4 (parents it3))
                          (if (not (member it4 templist )) 
                              (push it4 templist))
                      )
                  )
            
              )
          )

          (return-from parent-siblings templist)
      )
    )
)
(defun uncles (name)
    (return-from uncles (malefilter (parent-siblings name)))
)
(defun aunts (name)
    (return-from aunts (femalefilter (parent-siblings name)))
)
#|---------------------------------------------------------------------|
                  _   _ _ _     _ _                                    |
                 | \ | (_) |   | (_)                                   |
                 |  \| |_| |__ | |_ _ __   __ _ ___                    |
                 | . ` | | '_ \| | | '_ \ / _` / __|                   |
                 | |\  | | |_) | | | | | | (_| \__ \                   |
                 |_| \_|_|_.__/|_|_|_| |_|\__, |___/                   |
                                           __/ |                       |
                                          |___/                        |
#|---------------------------------------------------------------------|

    Function Names: niblings, nieces, nephews
    Purpose: find the Nieces and Nephews of a given name by finding
        the sibling's children, can be filtered by male or female    

|#--------------------------------------------------------------------|#
(defun niblings (name)
    (if (not (equal (nsearch name) nil))
      (let (templist)
          (dolist (temp (children name))
              (dolist (it0 (parents temp))
                  (dolist (it1 (siblings it0))
                      (dolist (it2 (children it1))        
                          (if (not (member it2 templist )) (push it2 templist))
                      )
                  )
              )
          )

          (return-from niblings templist)
      )
    )
)
(defun nieces (name)
    (return-from nieces (femalefilter (niblings name)))
)
(defun nephews (name)
    (return-from nephews (malefilter (niblings name)))
)
#|---------------------------------------------------------------------|
                  _____                _                               |
                 / ____|              (_)                              |
                | |     ___  _   _ ___ _ _ __  ___                     |
                | |    / _ \| | | / __| | '_ \/ __|                    |
                | |___| (_) | |_| \__ \ | | | \__ \                    |
                 \_____\___/ \__,_|___/_|_| |_|___/                    |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|
    
    Function Names: cousins, male-cousins, female-cousins
    Purpose: find the cousins of a given name by finding the parent's
        nieces and nephews, can be filtered by male or female

|#--------------------------------------------------------------------|#
(defun cousins (name)
    (if (not (equal (nsearch name) nil))
      (let (templist)
          (dolist (it1 (parents name))
              (dolist (it2 (niblings it1))
                  (if (not (member it2 templist )) (push it2 templist))
              )
          )
          (return-from cousins templist)
      )
    )
)

(defun male-cousins (name)
    (return-from male-cousins (malefilter (cousins name)))
)
(defun female-cousins (name)
    (return-from female-cousins (femalefilter (cousins name)))
)

#|---------------------------------------------------------------------|
    _____                               _             _                |
   |  __ \                             | |           | |               | 
   | |  | | ___  ___  ___ ___ _ __   __| | __ _ _ __ | |_ ___          |
   | |  | |/ _ \/ __|/ __/ _ \ '_ \ / _` |/ _` | '_ \| __/ __|         |
   | |__| |  __/\__ \ (_|  __/ | | | (_| | (_| | | | | |_\__ \         |
   |_____/ \___||___/\___\___|_| |_|\__,_|\__,_|_| |_|\__|___/         |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|
    
    Function Names: descendants, male-descendants, female-descendants
    Purpose: find the descendants of a given name by iterating down through
        the children and their children until there is no one left.
        Can be filtered by male or female.

|#--------------------------------------------------------------------|#
(defun descendants (name)
    (if (not (equal (nsearch name) nil))
      (let ((templist (children name)) temp2list)
          (dolist (it1 templist)
              (push it1 temp2list)
          )
          (dolist (it temp2list)
              (dolist (it2 (children it))
                  (setf temp2list (push it2 temp2list ))
              )
          )
          (return-from descendants temp2list)
      )
    )
)
(defun male-descendants (name)
    (return-from male-descendants (malefilter (descendants name)))
)
(defun female-descendants (name)
    (return-from female-descendants (femalefilter (descendants name)))
)
#|---------------------------------------------------------------------|
                                          _                            |
                /\                       | |                           |
               /  \   _ __   ___ ___  ___| |_ ___  _ __ ___            |
              / /\ \ | '_ \ / __/ _ \/ __| __/ _ \| '__/ __|           |
             / ____ \| | | | (_|  __/\__ \ || (_) | |  \__ \           |
            /_/    \_\_| |_|\___\___||___/\__\___/|_|  |___/           |
                                                                       |
                                                                       |
#|---------------------------------------------------------------------|
    
    Function Names: ancestors, male-ancestors, female-ancestors
    Purpose: find the ancestors of a given name by iterating up through
        the parents and their parents until there is no one left.
        Can be filtered by male or female.


|#--------------------------------------------------------------------|#
(defun ancestors (name)
    (if (not (equal (nsearch name) nil))
      (let ((templist (parents name)) temp2list)
          (dolist (it1 templist)
              (push it1 temp2list)
          )
          (dolist (it temp2list)
              (dolist (it2 (parents it))
                  (setf temp2list (push it2 temp2list ))
              )
          )
          (return-from ancestors temp2list)
      )
    )
)
(defun male-ancestors (name)
    (return-from male-ancestors (malefilter (ancestors name)))
)
(defun female-ancestors (name)
    (return-from female-ancestors (femalefilter (ancestors name)))
)




(buildDatabase (first *ARGS*))
